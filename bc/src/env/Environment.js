import React, {Component} from 'react';
import axios from 'axios';
import _ from 'lodash';
import Icon from "./Icon";

const WORKING_CLASS = 'working';
const ALERT_CLASS = 'alert';
const RUNNING_CONTAINER_STATUS = 'running';

const runningHost = 'localhost:3000';

export default class Environment extends Component {
    constructor(props) {
        super();

        this.receiveData = this.receiveData.bind(this);
        this.renderContent = this.renderContent.bind(this);
        this.fetchContainersStatus = this.fetchContainersStatus.bind(this);
        this.restart_container = this.restart_container.bind(this);
        this.markError = this.markError.bind(this);
        this.refreshData = this.refreshData.bind(this);
    }

    componentWillMount() {
        this.setState({'isReady': false});
        this.fetchContainersStatus();
    }

    renderError() {
        let desc = this.state.desc;
        return (
            <p className="error">
                <span className="error-title">Error occurred:</span>
                <p>
                    <code>{String(desc)}</code>
                </p>
            </p>);
    }

    renderLoading() {
        let host = this.props.host;
        return (
            <span>
                <div className="loader"/>
                Please wait, host: {host}
            </span>
        );
    }

    getTableRows(data) {
        let rows = _.map(data, (o => {
            let clazz = (o.state === RUNNING_CONTAINER_STATUS) ? WORKING_CLASS : ALERT_CLASS;
            return (
                <tr key={o.id} className={clazz}>
                    <td>{o.name}</td>
                    <td>{o.status}</td>
                    <td>
                        <Icon onClick={(event) => {
                            this.restart_container(o.id);
                        }} name="refresh"/>
                        <Icon onClick={(event) => {
                            this.open_logs(o.id, o.name);
                        }}
                              name="list-alt"/>
                        <Icon
                            onClick={(event) => {
                                this.stop_container(o.id);
                            }} name="stop"/>
                    </td>
                </tr>);
        }));
        rows.sort(obj => {
            if (obj.props.className === WORKING_CLASS) return 1; else return -1; //xDD jak biednie
        });
        return rows;
    }

    renderData() {
        let rows = this.getTableRows(this.state.data.data);
        return <table className="table">
            <thead className="thead-dark">
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Status</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            {rows}
            </tbody>
        </table>;

    }

    renderContent() {
        console.log('Will render with state', this.state);

        if (this.state.isError) {
            return this.renderError();
        }

        if (!this.state.isReady) {
            return this.renderLoading();
        }

        return this.renderData();
    }

    render() {
        return <div className='env'>
            <div className='hostname'>
                {this.props.envName} [30 sec] <Icon name="refresh" onClick={(event) => this.refreshData()}/>
            </div>
            {this.renderContent()}
        </div>;
    }

    fetchContainersStatus = () => {
        let host = this.props.host;
        let url = `http://${host}/list`;
        axios_get(url, this.receiveData, this.markError);
    };

    markError(response) {
        this.setState({'isError': true, 'desc': response});
    }

    receiveData(response) {
        this.setState({'isReady': true, 'data': response.data});
    }

    restart_container = (id) => {
        let host = this.props.host;
        let url = `http://${host}/restart/${id}`;
        this.setState({'isReady': false});
        axios_get(url, r => {
            this.fetchContainersStatus();
        }, this.markError);
    };

    refreshData = () => {
        this.setState({'isReady': false});
        this.fetchContainersStatus();
    }

    stop_container = (id) => {
        let host = this.props.host;
        let url = `http://${host}/stop/${id}`;
        this.setState({'isReady': false});
        axios_get(url, r => {
            this.fetchContainersStatus();
        }, this.markError);
    };

    open_logs = (id, name) => {
        let host = this.props.host;
        window.open(`http://${runningHost}/logs/${host}/${id}/${name}`);
    }
}

function axios_get(url, success, error) {
    axios.get(url).then(response => {
        success(response);
    }, response => {
        error(response);
    });
}