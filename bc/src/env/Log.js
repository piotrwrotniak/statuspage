import React, {Component} from 'react';
import io from 'socket.io-client';
import $ from 'jquery';

export default class Log extends Component {

    componentWillMount() {
        let params = this.props.match.params;
        this.setState(
            {
                host: params.host,
                container_id: params.id,
                name: params.name
            }
        );
    }

    componentDidMount() {
        let socket = io('http://' + this.state.host + '/test');
        let content = $('#content');

        socket.on('newData', function(msg) {
            content.append(msg.data + "<br>");
            //window.scrollTo(0, document.body.scrollHeight);
        });
        socket.emit('listenLogs', {data: this.state.container_id});
    }

    render() {
        let host = this.state.host;
        let container_id = this.state.container_id;
        let name = this.state.name;
        return (
            <div style={{padding:"10px"}}>
                <p style={{fontWeight:'bold'}}>
                    Logs for {name}:
                </p>
                <p>
                    Feeding live data from host: <i> {host} </i>,
                    <br/> container_id: <i> {container_id} </i>
                </p>
                <p className="code-content">
                    <code id="content"/>
                </p>
            </div>
        );
    }
}