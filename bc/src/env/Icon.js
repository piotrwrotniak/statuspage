import React, { Component } from 'react';


export default class Icon extends Component {
    render() {
        return (
            <span style={{cursor:"pointer"}} onClick={this.props.onClick}>
                <i className={"fa fa-" + this.props.name} style={{padding: "6px"}} aria-hidden="true"/>
            </span>
        )
    }}
