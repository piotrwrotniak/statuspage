import React, {Component} from 'react';
import Environment from './env/Environment'
import logo from './logo.svg';
import './App.css';
import _ from 'lodash';
import Modal from "./env/Modal";

class App extends Component {

    render() {
        let hosts = [
            {host: "localhost:5000", name: "Dev"},
            {host: "fppc:5000", name: "Test"},
            {host: "fppc:5000", name: "QA"},
            {host: "fppc:5000", name: "Stage"}
        ];
        let envs = _.map(hosts, (e => {
            return (
                <td key={e.name} style={{minWidth: "360px", maxWidth:"360px"}} >
                    <Environment host={e.host} envName={e.name}/>
                </td>
            );
        }));

        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1 className="App-title">Status Page</h1>
                </header>
                <div className="App-intro">
                    <table>
                        <tbody>
                            <tr>
                                {envs}
                            </tr>
                        </tbody>
                    </table>
                </div>
                <Modal/>
            </div>
        );
    }
}

export default App;
