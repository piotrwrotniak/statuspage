import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from "react-router-dom";
import {Switch, Route} from "react-router-dom";
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import Log from "./env/Log";

ReactDOM.render(
        <BrowserRouter forceRefresh={true}>
            <Switch>
                <Route exact path="/" component={App} />
                <Route path="/logs/:host/:id/:name" component={Log} />
            </Switch>
        </BrowserRouter>
    , document.getElementById('root'));

registerServiceWorker();