# StatusPage for Docker-based envs

### Status: Beta

Simple application to control multiple environments, allows to list, start, stop, restart container.
To be continued if any interest found.

Streams logs using SocketIO. 
Daemon written in Python (REST Api in Flask).
Frontend created from ReactJS boilerplate.

Known TODO: 
- attach to logs after container restart (and start sending the logs on the same socket) (backend)
- resolve times + colors from Spring containers and colorize the syntax (frontend)
- room for improvement for gui :)
- refactor, add support for docker swarm

![Screenshot](https://i.imgur.com/qopoisZ.jpg "Screen")
