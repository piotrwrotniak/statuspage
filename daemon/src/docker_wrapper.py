import docker
from MultipleContainersFoundException import MultipleContainersFoundException

client = docker.from_env()
raw_client = docker.APIClient(base_url='unix://var/run/docker.sock')

def docker_get_containers():
    return {c.id : c for c in client.containers.list(all=True)}

def docker_get_containers_list_raw():
    containers = raw_client.containers(all=True)
    return {c['Id']: {"name": c['Names'][0][1:], "status":c['Status'], "state":c['State'], "id":c['Id']} for c in containers}

def find_container(id_or_name):
    containers = docker_get_containers()
    matches = []
    if id_or_name in containers:
        matches.append(containers[id_or_name])
    for c in containers.items():
        if id_or_name in c[1].name:
            matches.append(c[1])
    if len(matches) == 1:
        return matches[0]
    else:
        names = [m.name for m in matches]
        raise MultipleContainersFoundException(names)
