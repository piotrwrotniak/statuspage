class MultipleContainersFoundException(Exception):
    def __init__(self, names):
        self.names = names

    def get_msg(self):
        return 'Multiple containers matches criteria: ' + (', '.join(self.names))
