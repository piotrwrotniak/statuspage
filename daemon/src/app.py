from flask import Flask, redirect, url_for, request, jsonify
from docker_wrapper import *
from flask_cors import CORS
from flask_socketio import SocketIO, emit, send

app = Flask(__name__)
CORS(app)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)

EMPTY_RESPONSE = {'status': 'OK'}

def return_err(msg):
    return jsonify({'status' : 'error', 'desc' : msg})

@app.errorhandler(MultipleContainersFoundException)
def handle_multiple_containers_found_exception(e):
    return return_err(e.get_msg())

@app.route('/list')
def containers_list():
    data = docker_get_containers_list_raw()
    return jsonify({
        "count": len(data),
        "data": data
    })

@app.route('/restart/<id_or_name>')
def restart_container(id_or_name):
    container = find_container(id_or_name)
    container.restart()
    return jsonify(EMPTY_RESPONSE)

@app.route('/logs/<id_or_name>')
def get_logs(id_or_name, lines = 500):
    container = find_container(id_or_name)
    logs = container.logs(tail=lines)
    return jsonify({'status': 'OK', 'data': logs})

@app.route('/stop/<id_or_name>')
def stop_container(id_or_name, lines = 500):
    container = find_container(id_or_name)
    container.stop()
    return jsonify(EMPTY_RESPONSE)

@socketio.on('listenLogs', namespace='/test')
def test_message(id_or_name):
    print(id_or_name)
    container = find_container(id_or_name['data'])
    logs = container.logs(tail=500, timestamps=True, stream=True, follow=True)
    for line in logs:
        emit('newData', {'status': 'OK', 'data': str(line.decode('utf-8'))});

socketio.run(app, host='0.0.0.0')
# app.run(host= '0.0.0.0')
